#!/usr/bin/env python
# coding: utf-8

# In[39]:


#Комментарий:
#по результатам первого теста со всей выборкой мы узнали, что наши выборки "поломаны" (P>0.05)
#затем мы посмотрели распределение конвертруемости и увидели значительную разницу в версии приложения 2.8.0
#затем, мы провели АА тест по версиям приложения, и действительно версия показала значительное расхождение.
#однако и 2 другие версии показали p>0.05, тем не менее при повторении этот параметр постоянно пересекал эту черту, в связи с чем было решено их оставить
#в результате, после исключения из выборки пользователей из группы 2.8.0 результат A/A теста показал p<0.05, соответстветственно, основная поломка находилась именно там


# In[2]:


import numpy as np
import pandas as pd
from scipy import stats
import matplotlib.pyplot as plt

from tqdm.auto import tqdm


# In[3]:


df = pd.read_csv('/mnt/HC_Volume_18315164/home-jupyter/jupyter-st-popov/statististics/DATA/hw_aa.csv', sep=';')


# In[4]:


df = df.dropna()


# In[17]:


var_1 = df.query('experimentVariant == 1')
var_1 = var_1['purchase']
var_0 = df.query('experimentVariant == 0')
var_0 = var_0['purchase']


# In[18]:


n = 100000
simulations = 1000
n_s = 1000
res = []

# Запуск симуляций A/A теста
for i in tqdm(range(simulations)):
    s1 = var_1.sample(n_s, replace = False).values
    s2 = var_0.sample(n_s, replace = False).values
    res.append(stats.ttest_ind(s1, s2, equal_var = False)[1]) # сохраняем pvalue

plt.hist(res, bins = 50)
plt.style.use('ggplot')
plt.xlabel('pvalues')
plt.ylabel('frequency')
plt.title("Histogram of ttest A/A simulations ")
plt.show()

# Проверяем, что количество ложноположительных случаев не превышает альфа
sum(np.array(res) <0.05) / simulations


# In[3]:


df.groupby(['version', 'experimentVariant'])     .agg({'purchase': 'mean'})


# In[19]:


#сплитим данные чтобы можно было потом выяснить какая группа является проблемной
v2_8_0 = df.query("experimentVariant == 0 and version == 'v2.8.0'")
v2_8_0 = v2_8_0['purchase']
v2_8_1 = df.query("experimentVariant == 1 and version == 'v2.8.0'")
v2_8_1 = v2_8_1['purchase']
v2_9_0 = df.query("experimentVariant == 0 and version == 'v2.9.0'")
v2_9_0 = v2_9_0['purchase']
v2_9_1 = df.query("experimentVariant == 1 and version == 'v2.9.0'")
v2_9_1 = v2_9_1['purchase']
v3_7_0 = df.query("experimentVariant == 0 and version == 'v3.7.4.0'")
v3_7_0 = v3_7_0['purchase']
v3_7_1 = df.query("experimentVariant == 1 and version == 'v3.7.4.0'")
v3_7_1 = v3_7_1['purchase']
v3_8_0 = df.query("experimentVariant == 0 and version == 'v3.8.0.0'")
v3_8_0 = v3_8_0['purchase']
v3_8_1 = df.query("experimentVariant == 1 and version == 'v3.8.0.0'")
v3_8_1 = v3_8_1['purchase']


# In[30]:


#проверяем данные по группам
n = 1000
simulations = 1000
n_s = 1000
res = []

# Запуск симуляций A/A теста
for i in tqdm(range(simulations)):
    s1 = v2_8_0.sample(n_s, replace = False).values
    s2 = v2_8_1.sample(n_s, replace = False).values
    res.append(stats.ttest_ind(s1, s2, equal_var = False)[1]) # сохраняем pvalue

plt.hist(res, bins = 50)
plt.style.use('ggplot')
plt.xlabel('pvalues')
plt.ylabel('frequency')
plt.title("Histogram of ttest A/A simulations ")
plt.show()

# Проверяем, что количество ложноположительных случаев не превышает альфа
sum(np.array(res) <0.05) / simulations


# In[31]:


n = 1000
simulations = 1000
n_s = 1000
res = []

# Запуск симуляций A/A теста
for i in tqdm(range(simulations)):
    s1 = v2_9_0.sample(n_s, replace = False).values
    s2 = v2_9_1.sample(n_s, replace = False).values
    res.append(stats.ttest_ind(s1, s2, equal_var = False)[1]) # сохраняем pvalue

plt.hist(res, bins = 50)
plt.style.use('ggplot')
plt.xlabel('pvalues')
plt.ylabel('frequency')
plt.title("Histogram of ttest A/A simulations ")
plt.show()

# Проверяем, что количество ложноположительных случаев не превышает альфа
sum(np.array(res) <0.05) / simulations


# In[32]:


n = 1000
simulations = 1000
n_s = 1000
res = []

# Запуск симуляций A/A теста
for i in tqdm(range(simulations)):
    s1 = v3_7_0.sample(n_s, replace = False).values
    s2 = v3_7_1.sample(n_s, replace = False).values
    res.append(stats.ttest_ind(s1, s2, equal_var = False)[1]) # сохраняем pvalue

plt.hist(res, bins = 50)
plt.style.use('ggplot')
plt.xlabel('pvalues')
plt.ylabel('frequency')
plt.title("Histogram of ttest A/A simulations ")
plt.show()

# Проверяем, что количество ложноположительных случаев не превышает альфа
sum(np.array(res) <0.05) / simulations


# In[35]:


n = 1000
simulations = 1000
n_s = 1000
res = []

# Запуск симуляций A/A теста
for i in tqdm(range(simulations)):
    s1 = v3_8_0.sample(n_s, replace = False).values
    s2 = v3_8_1.sample(n_s, replace = False).values
    res.append(stats.ttest_ind(s1, s2, equal_var = False)[1]) # сохраняем pvalue

plt.hist(res, bins = 50)
plt.style.use('ggplot')
plt.xlabel('pvalues')
plt.ylabel('frequency')
plt.title("Histogram of ttest A/A simulations ")
plt.show()

# Проверяем, что количество ложноположительных случаев не превышает альфа
sum(np.array(res) <0.05) / simulations


# In[5]:


#убираем проблемную группу
cor_var_1 = df.query("experimentVariant == 1 and version != 'v2.8.0'")
cor_var_1 = cor_var_1['purchase']
cor_var_0 = df.query("experimentVariant == 0 and version != 'v2.8.0'")
cor_var_0 = cor_var_0['purchase']


# In[6]:


#проводим AA тест без "поломанной группы"
n = 1000
simulations = 1000
n_s = 1000
res = []

# Запуск симуляций A/A теста
for i in tqdm(range(simulations)):
    s1 = cor_var_1.sample(n_s, replace = False).values
    s2 = cor_var_0.sample(n_s, replace = False).values
    res.append(stats.ttest_ind(s1, s2, equal_var = False)[1]) # сохраняем pvalue

plt.hist(res, bins = 50)
plt.style.use('ggplot')
plt.xlabel('pvalues')
plt.ylabel('frequency')
plt.title("Histogram of ttest A/A simulations ")
plt.show()

# Проверяем, что количество ложноположительных случаев не превышает альфа
sum(np.array(res) <0.05) / simulations


# In[ ]:




